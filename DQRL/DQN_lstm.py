# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import print_function


import numpy as np
import pandas as pd
pd.set_option('display.width',250)
import DQRL.backtest as twp
from matplotlib import pyplot as plt
from talib.abstract import *
from DQRL.DATA_PATH import *

def load_data(test=False):
    prices = pd.read_pickle(DAILYDATAPATH+'rb_M_adjusted.pkl')
    prices.volume=prices.volume.astype(float)
    prices=prices[prices.open!=0]
    x_train = prices.iloc[:100,]
    x_test= prices.iloc[:1000]
    if test:
        return x_test
    else:
        return x_train

#Initialize first state, all items are placed deterministically
def init_state(indata, test=False):
    close = indata['close']
    nextopen=indata['open'].shift(-1)
    t3=T3(indata)

    #--- Preprocess data
    DF=pd.DataFrame(data={0:t3})
    DF=((DF-DF.rolling(window=20).mean())/DF.rolling(window=20).std()).dropna(axis=0)
    xdata= np.expand_dims(DF.values[:-1], axis=1)
    timeindex = DF.index[:-1]
    nextopen=nextopen[timeindex]
    state = xdata[0:1, 0:1, :]
    
    return state, xdata, nextopen,timeindex

#Take Action
def take_action(state, xdata, qval,action, signal, time_step):
    #this should generate a list of trade signals that at evaluation time are fed to the backtester
    #the backtester should get a list of trade signals and a list of price data for the asset
    
    #make necessary adjustments to state and then return it
    time_step += 1
    
    #if the current iteration is the last state ("terminal state") then set terminal_state to 1
    if time_step + 1 == xdata.shape[0]:
        state = xdata[time_step:time_step+1, 0:1, :]
        terminal_state = 1
        signal.iloc[time_step] = 0

        return state, time_step, signal, terminal_state

    #move the market data window one step forward
    state = xdata[time_step:time_step+1, 0:1, :]
    #take action
    if action == 0:
        signal.iloc[time_step] = 10000
    elif action == 1 :
        signal.iloc[time_step] = -10000
    else:
        signal.iloc[time_step] = 0
    terminal_state = 0

    return state, time_step, signal, terminal_state

#Get Reward, the reward is returned at the end of an episode
def get_reward(new_state, time_step, action, price_data, signal, terminal_state,modelAccuracy, eval=False, epoch=0,p=False):
    reward = 0
    signal.fillna(value=0, inplace=True)

    if eval == False:
        shares=signal.iloc[time_step]/price_data.iloc[time_step-1]
        changePosition=signal.diff().abs().iloc[time_step]/10000
        pnl = (price_data.iloc[time_step] - price_data.iloc[time_step-1])*shares-changePosition*5
        reward=int((pnl/10000)>0.002)

    if terminal_state == 1 and eval == True:
        #save a figure of the test set
        bt = twp.Backtest(price_data, signal, signalType='capital',roundShares=False,slippage=0.0005)
        print(bt.data)
        cumpnl=bt.cumpnl
        reward=cumpnl.dropna()[-1]-1
        train_test_split=signal.index[buffer]
        trainlable_x=signal.index[buffer-10]
        testlable_x=signal.index[buffer+10]
        cumpnl.plot()
        plt.figure(figsize=(10,20))
        plt.axvline(x=train_test_split, color='black', linestyle='--')
        bt.plotTrades()
        plt.axvline(x=train_test_split, color='black', linestyle='--')
        plt.text(trainlable_x, 1, 'training data')
        plt.text(testlable_x, 1, 'test data')
        plt.suptitle(str(epoch))
        plt.show()
        # plt.savefig('plt/'+str(epoch)+'.png', bbox_inches='tight', pad_inches=1, dpi=72)
        # plt.close('all')
        # print(time_step, terminal_state, eval, trainReward,testReward)
    if p:
        print(time_step, terminal_state, eval,action, reward,pnl,modelAccuracy)
    return reward
def evaluate_Q(eval_data, eval_model, price_data, epoch=0):
    #This function is used to evaluate the performance of the system each epoch, without the influence of epsilon and random actions
    state, xdata, price_data,timeindex = init_state(eval_data)
    signal = pd.Series(index=timeindex)
    status = 1
    terminal_state = 0
    time_step = 1
    while(status == 1):
        #We start in state S
        #Run the Q function on S to get predicted reward values on all the possible actions
        qval = eval_model.predict(state, batch_size=1)
        action = (np.argmax(qval))
        print(qval,action)
        #Take action, observe new state S'
        new_state, time_step, signal, terminal_state = take_action(state, xdata, action, signal, time_step)
        #Observe reward
        eval_reward = get_reward(new_state, time_step, action, price_data, signal, terminal_state, eval=True, epoch=epoch)
        state = new_state
        if terminal_state == 1: #terminal state
            status = 0

    return eval_reward




def get_miniBatch_XY(minibatch,model,nb_action,gamma,alpha,terminal_state,norm=False):
    X_train = []
    y_train = []
    for memory in minibatch:
        # Get max_Q(S',a)
        old_state, action, reward, new_state = memory
        old_qval = model.predict(old_state, batch_size=1)
        newQ = model.predict(new_state, batch_size=1)
        maxQ = np.max(newQ)
        y = np.zeros((1, nb_action))
        y[:] = old_qval[:]
        if terminal_state == 0:  # non-terminal state
            update = y[0][action]+alpha*(reward + gamma * maxQ-y[0][action])
        else:  # terminal state
            update = reward
        y[0][action] = update
        # print(time_step, reward, terminal_state)
        X_train.append(old_state)
        y_train.append(y.reshape(nb_action, ))
    if norm:
        X_train-=np.mean(X_train, axis = 0)
        X_train /= np.std(X_train, axis=0)  # normalize
    X_train = np.squeeze(np.array(X_train), axis=(1))
    y_train = np.array(y_train)
    return X_train, y_train

def shuffle_weights(model, weights=None):
    """Randomly permute the weights in `model`, or the given `weights`.
    This is a fast approximation of re-initializing the weights of a model.
    Assumes weights are distributed independently of the dimensions of the weight tensors
      (i.e., the weights have the same distribution along each dimension).
    :param Model model: Modify the weights of the given model.
    :param list(ndarray) weights: The model's weights will be replaced by a random permutation of these weights.
      If `None`, permute the model's current weights.
    """
    if weights is None:
        weights = model.get_weights()
    weights = [np.random.permutation(w.flat).reshape(w.shape) for w in weights]
    # Faster, but less random: only permutes along the first dimension
    # weights = [np.random.permutation(w) for w in weights]
    model.set_weights(weights)


#This neural network is the the Q-function, run it like this:
#model.predict(state.reshape(1,64), batch_size=1)

seed=1335
np.random.seed(seed)

def get_signal(buffer):
    from keras.models import Sequential
    from keras.layers.core import Dense, Dropout, Activation
    from keras.layers.recurrent import LSTM
    from keras.optimizers import RMSprop, Adam, Adagrad, SGD
    from keras.regularizers import L1L2
    from keras.initializers import glorot_uniform,orthogonal
    import random
    from sklearn.cross_validation import StratifiedKFold
    batch_size = 1
    num_features = 1
    nb_action=3
    random_glo=glorot_uniform(seed=seed)
    random_orth=orthogonal(seed=seed)
    model = Sequential()
    model.add(LSTM(10,input_shape=(1, num_features),kernel_initializer=random_glo,recurrent_initializer=random_orth,return_sequences=True,stateful=False,kernel_regularizer=L1L2(l1=0.01,l2=0.01)))#,kernel_regularizer=L1L2(l1=0.01,l2=0.01)
    model.add(Dropout(0.5,seed=seed))
    model.add(LSTM(10,input_shape=(1, num_features),kernel_initializer=random_glo,recurrent_initializer=random_orth,return_sequences=False,stateful=False,kernel_regularizer=L1L2(l1=0.01,l2=0.01)))#,kernel_regularizer=L1L2(l1=0.01,l2=0.01)
    model.add(Dropout(0.5,seed=seed))
    model.add(Dense(nb_action, kernel_initializer=random_glo))
    model.add(Activation('softmax')) #linear output so we can have range of real-valued outputs


    adam = Adam()
    model.compile(loss='mse', optimizer=adam,metrics=['accuracy'])
    # initial_weights = model.get_weights()
    # print('Initial weights:\n', initial_weights)




    indata = load_data() #get train set data
    test_data = load_data(test=True)
    epochs = 1
    gamma = 0.1 #since the reward can be several time steps away, make gamma high
    alpha=0.5 #learning rate, might be adaptive in the future version
    epsilon = 1
    buffer = buffer
    batchSize = buffer

    #stores tuples of (S, A, R, S')
    replay = []

    for i in range(epochs):
        if i == epochs-1: #the last epoch, use test data set
            indata = test_data
            state, xdata, price_data,timeindex = init_state(indata, test=True)
        else:
            state, xdata, price_data,timeindex = init_state(indata)
        signal = pd.Series(index=timeindex)
        status = 1
        terminal_state = 0
        time_step = 0
        modelAccuracy=0
        #while game still in progress
        while(status == 1):
            #We are in state S
            #Let's run our Q function on S to get Q values for all possible actions
            qval = model.predict(state,verbose=0)
            if (random.random() < epsilon): #choose random action
                action = np.random.randint(0,nb_action) #assumes 2 different actions
            else: #choose best action from Q(s,a) values
                action = (np.argmax(qval))
            #Take action, observe new state S'
            new_state, time_step, signal, terminal_state = take_action(state, xdata, qval,action, signal, time_step)
            #Observe reward
            reward = get_reward(new_state, time_step, action, price_data, signal, terminal_state,modelAccuracy)

            #Experience replay storage
            if (len(replay) < buffer): #if buffer not filled, add to it
                replay.append((state, action, reward, new_state))

            else: #if buffer full, overwrite old values
                del replay[0]
                replay.append((state, action, reward, new_state))
                #randomly sample our experience replay memory
                # minibatch = random.sample(replay, batchSize)
                X_train,Y_train=get_miniBatch_XY(replay, model,nb_action,gamma,alpha,terminal_state)
                kfold = StratifiedKFold(np.ones(len(Y_train)),n_folds=5, shuffle=True, random_state=seed)
                for train, test in kfold:
                    # shuffle_weights(model)
                    model.fit(X_train[train], Y_train[train], batch_size=10, epochs=5, verbose=0,shuffle=False)
                    modelAccuracy = model.evaluate(X_train[test], Y_train[test], batch_size=batch_size, verbose=2)[1]

                # use below epsilow decay when epochs=1
                if epsilon > 0.1 and epochs==1:
                    epsilon -= (1.0 / 100)
            state = new_state
            if terminal_state == 1: #if reached terminal state, update epoch status
                status = 0


    bt = twp.Backtest(price_data, signal, signalType='capital', roundShares=False, slippage=0.0005)
    cumpnl = bt.cumpnl.iloc[buffer:-1] / bt.cumpnl.iloc[buffer]
    annualret = (cumpnl.iloc[-2] ** (250 / len(cumpnl)) - 1)
    annualstd = np.sqrt(250) * cumpnl.pct_change().std()
    sharpe = (annualret / annualstd)
    print('Sharpe ratio = %s,average holding period = %s' % (sharpe,bt.avgHP))
    return price_data,signal



import progressbar

if __name__=='__main__':
    SignalDF=pd.DataFrame()
    buffer=50
    bar=progressbar.ProgressBar()
    for i in bar(range(10)):
        price_data, sig=get_signal(buffer=buffer)
        SignalDF[i]=sig
    signal=SignalDF.mode(axis=1)
    if signal.shape[1]>1:
        mask=signal.iloc[:,1].notnull()
        signal.iloc[:,0].loc[mask]=0
    signal=signal.iloc[:,0]
    print(signal)
    bt = twp.Backtest(price_data, signal, signalType='capital', roundShares=False, slippage=0.0005)
    print(bt.data)
    bt.data.to_csv('bt.csv')
    cumpnl = bt.cumpnl.iloc[buffer:-1] / bt.cumpnl.iloc[buffer]
    annualret = (cumpnl.iloc[-2] ** (250 / len(cumpnl)) - 1)
    annualstd = np.sqrt(250) * cumpnl.pct_change().std()
    sharpe = (annualret / annualstd)
    print('Sharpe ratio = %s' % sharpe)
    print('average holding period = %s' % bt.avgHP)
    #
    train_test_split = signal.index[buffer]
    trainlable_x = signal.index[buffer-10]
    testlable_x = signal.index[buffer + 10]

    plt.figure()
    plt.subplot(3,1,1)
    plt.axvline(x=train_test_split, color='black', linestyle='--')
    plt.text(trainlable_x, 1, 'training data')
    plt.text(testlable_x, 1, 'test data')
    bt.plotTrades()
    plt.subplot(3,1,2)
    plt.title('full cumpnl')
    bt.cumpnl.plot()
    plt.axvline(x=train_test_split, color='black', linestyle='--')
    plt.text(trainlable_x, 1, 'training data')
    plt.text(testlable_x, 1, 'test data')
    plt.subplot(3,1,3)
    plt.title('test cumpnl')
    bt.cumpnl[buffer:].pct_change().add(1).cumprod().plot()
    # plt.savefig('plt/summary'+'.png', bbox_inches='tight', pad_inches=1, dpi=72)
    plt.show()





