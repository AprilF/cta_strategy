import pandas as pd
import os
import progressbar
pd.set_option('display.width',250)


def csvTOpkl():
    datapath='C:\\Users\\April\\Desktop\\TickData_comFut\\'
    newdatapath='C:\\Users\\April\\Desktop\\pkldata\\'
    files=os.listdir(datapath)
    newfiles=os.listdir(newdatapath)
    bar=progressbar.ProgressBar()
    for file in bar(files):
        newfilename=file.split('.')[0]+'.pkl'
        if newfilename in newfiles:
            continue
        data = pd.read_csv(datapath + file, header=0, index_col=0)
        data.to_pickle(newdatapath+newfilename)

def tickTOmin():
    datapath = 'C:\\Users\\April\\Desktop\\pkldata\\'
    newdatapath = 'C:\\Users\\April\\Desktop\\minData\\'
    files = os.listdir(datapath)
    newfiles = os.listdir(newdatapath)
    bar = progressbar.ProgressBar()
    for file in bar(files):
        if file in newfiles:
            continue
        data = pd.read_pickle(datapath + file)
        data.ExchTime = pd.to_datetime(data.ExchTime)
        data = data.reset_index(drop=False).set_index('ExchTime')
        minPrice = data.LastPrice.groupby([pd.Grouper(freq='1Min'),data.InstrumentID]).ohlc()
        minVolume = data.Volume.groupby([pd.Grouper(freq='1Min'),data.InstrumentID]).sum().to_frame('Volume')
        openInterest=data.OpenInterest.groupby([pd.Grouper(freq='1Min'),data.InstrumentID]).last().to_frame('OpenInterest')
        minData = pd.concat([minPrice, minVolume,openInterest], axis=1)
        minData=minData.reset_index(level=-1,drop=False)
        minData.to_pickle(newdatapath+file)


if __name__=='__main__':
    tickTOmin()


