import pandas as pd
import numpy as np
import matplotlib.pylab as plt
import progressbar
from DATA_PATH import *

pd.set_option('display.width',250)



def P_factor(contractName,windowsize=60,threshold=0.1):
    data = pd.read_pickle(datapath + contractName+'_M.pkl')
    minReturn=(data.close/data.open-1).to_frame('ret')
    S_factor=(minReturn.abs().ret*10000/data.Volume.apply(np.sqrt)).to_frame('S')
    mix=pd.concat([minReturn,S_factor,data],axis=1)
    P_factor=pd.DataFrame(columns=['P'])
    bar=progressbar.ProgressBar()
    for i in bar(range(len(mix)-windowsize)):
        tmpmix=mix[i:windowsize+i]
        tmpmix=tmpmix.sort_values(by='S',ascending=False)
        cumvol=tmpmix.Volume.cumsum()/tmpmix.Volume.sum()
        thisP=tmpmix.ret[cumvol<threshold].abs().sum()
        P_factor.loc[tmpmix.index.max()]=thisP
    P_factor.to_pickle('P_factor.pkl')

def Q_factor(contractName,windowsize=60,threshold=0.1):
    data=pd.DataFrame()
    for year in range(2014,2018):
        tmpdata=pd.read_pickle(MINDATAPATH+contractName+'_individual_'+str(year)+'.pkl')
        data=pd.concat([data,tmpdata])
    M_data=pd.read_pickle(MINDATAPATH+contractName+'_M.pkl')
    dOI=data.groupby(data.InstrumentID).apply(lambda x: x.OpenInterest.diff().abs()).to_frame('dOI')
    dOI=dOI.reset_index(level=0,drop=False)
    sumdOI=dOI.groupby(level=0).sum()
    sumVol=data.groupby(level=0).Volume.sum()
    S_factor=(sumdOI.dOI/sumVol.apply(np.sqrt)).to_frame('S')
    mix=pd.concat([sumdOI,S_factor,M_data],join='inner',axis=1)
    Q_factor = pd.DataFrame(columns=['Q'])
    bar = progressbar.ProgressBar()
    for i in bar(range(len(mix) - windowsize)):
        tmpmix = mix[i:windowsize + i]
        tmpmix = tmpmix.sort_values(by='S', ascending=False)
        cumvol = tmpmix.Volume.cumsum() / tmpmix.Volume.sum()
        thisQ = tmpmix.dOI[cumvol < threshold].abs().sum()
        Q_factor.loc[tmpmix.index.max()] = thisQ
    Q_factor.to_pickle('Q_factor.pkl')

def P_factor_daily(contractName,threshold=0.1):
    data = pd.read_pickle(MINDATAPATH + contractName+'_M_adjusted.pkl')
    minReturn=(data.close/data.open-1).to_frame('ret')
    S_factor=(minReturn.abs().ret*10000/data.volume.apply(np.sqrt)).to_frame('S')
    mix=pd.concat([minReturn,S_factor,data],axis=1)
    P_factor=pd.DataFrame(columns=['P'])
    bar=progressbar.ProgressBar()
    mix['tradingday_Label']=(mix.index-pd.Timedelta('3H')).date
    grouped=mix.groupby(mix.tradingday_Label)
    for tradingdate,group in bar(grouped):
        thisgroup=group.sort_values(by='S',ascending=False)
        cumvol=thisgroup.volume.cumsum()/thisgroup.volume.sum()
        thisP=thisgroup.ret[cumvol<threshold].abs().sum()
        P_factor.loc[tradingdate,'P']=thisP
    P_factor.to_pickle('P_factor_daily.pkl')

def Q_factor_daily(contractName,threshold=0.1):
    data=pd.DataFrame()
    for year in range(2014,2018):
        tmpdata=pd.read_pickle(MINDATAPATH+contractName+'_individual_'+str(year)+'.pkl')
        data=pd.concat([data,tmpdata])
    M_data=pd.read_pickle(MINDATAPATH+contractName+'_M_adjusted.pkl')
    dOI=data.groupby(data.InstrumentID).apply(lambda x: x.openinterest.diff().abs()).to_frame('dOI')
    dOI=dOI.reset_index(level=0,drop=False)
    sumdOI=dOI.groupby(level=0).sum()
    sumVol=data.groupby(level=0).volume.sum()
    S_factor=(sumdOI.dOI/sumVol.apply(np.sqrt)).to_frame('S')
    mix=pd.concat([sumdOI,S_factor,M_data],join='inner',axis=1)
    Q_factor = pd.DataFrame(columns=['Q'])
    bar = progressbar.ProgressBar()
    mix['tradingday_Label'] = (mix.index - pd.Timedelta('3H')).date
    grouped = mix.groupby(mix.tradingday_Label)
    for tradingdate, group in bar(grouped):
        thisgroup = group.sort_values(by='S', ascending=False)
        cumvol = thisgroup.volume.cumsum() / thisgroup.volume.sum()
        thisQ = thisgroup.dOI[cumvol < threshold].abs().sum()
        Q_factor.loc[tradingdate,'Q'] = thisQ
    Q_factor.to_pickle('Q_factor_daily.pkl')

if __name__=='__main__':
    contractName = 'rb'
    P_factor_daily(contractName)
    Q_factor_daily(contractName)
    # P_factor(contractName)
    # Q_factor(contractName)


