import pandas as pd
import numpy as np
from FZJG.DATA_PATH import *

def PQ_strat():
    getP=pd.read_pickle(DAILYDATAPATH+'P_factor_daily.pkl')
    getQ=pd.read_pickle(DAILYDATAPATH+'Q_factor_daily.pkl')
    mixSig=pd.concat([getP,getQ],axis=1,join='inner')

    mixSig=mixSig-mixSig.rolling(window=5).mean()
    trades=(mixSig.P*mixSig.Q)<0
    trades=trades.astype(int).replace(0,-1)
    return trades

def Dual_Trust(assetName,N_day_param,K1,K2):
    rawdata=pd.read_pickle(MINDATAPATH+assetName+'_M_origin.pkl')
    data=rawdata.assign(tradingdate=(rawdata.index-pd.Timedelta('3H')).date)
    data=data.reset_index(drop=False).set_index('tradingdate')
    datagroups=data.groupby(level=0)
    dailydata=pd.concat([datagroups.apply(lambda x: x.open.iloc[0]).to_frame('open'),
                         datagroups.apply(lambda x: x.high.max()).to_frame('high'),
                         datagroups.apply(lambda x: x.low.min()).to_frame('low'),
                         datagroups.apply(lambda x: x.close.iloc[-1]).to_frame('close')],axis=1)
    HH=dailydata.high.rolling(window=N_day_param).max()
    HC=dailydata.close.rolling(window=N_day_param).max()
    LL=dailydata.low.rolling(window=N_day_param).min()
    LC=dailydata.close.rolling(window=N_day_param).min()
    Rng=pd.concat([(HH-LC).to_frame('tmp1'),(HC-LL).to_frame('tmp2')],axis=1).max(axis=1)
    BuyLine=dailydata.open+(K1*Rng).shift(1)
    SellLine=dailydata.open-(K2*Rng).shift(1)
    data=data.set_index('datetime',append=True)
    BuySignal=data.open>(BuyLine.reindex(index=data.index,level=0))
    SellSignal=data.open<(SellLine.reindex(index=data.index,level=0))
    signals=BuySignal*1+SellSignal*-1
    signals.index=signals.index.droplevel(0)
    return signals
