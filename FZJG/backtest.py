import matplotlib.pylab as plt
from FZJG.strategy import *
import progressbar
from FZJG.DATA_PATH import *
import pandas as pd
def getBackTestData(backtest_freq,contractName):
    if backtest_freq=='daily':
        PATH=DAILYDATAPATH
    elif backtest_freq=='min':
        PATH=MINDATAPATH
    else:
        raise Exception
    BTData=pd.read_pickle(PATH+contractName+'_M_origin.pkl')
    BTData=BTData.assign(nextopen=BTData.open.shift(-1))
    return BTData


def getPosition(leverage,signals):
    positionSeries=leverage*signals
    return positionSeries


def tradeDetail(minIndex,InitialDetail):
    tradeDF=pd.DataFrame(index=minIndex,columns=InitialDetail.index)
    tradeDF.iloc[0]=InitialDetail
    return tradeDF

def CalRealTradingDetail(positionSeries, BTData,Init,Fee,contractMultiplier,ExchMarginRatioSeries=None, Slippage=0.0005,stoploss=0.01,stopwin=0.005):

    openSeries =BTData.open
    if ExchMarginRatioSeries is None:
        ExchMarginRatioSeries = pd.Series(0.10, index=positionSeries.index)

    allMinIndex=positionSeries.index
    dateIndex=pd.Series(data=allMinIndex.date).unique()
    bar=progressbar.ProgressBar()
    startMinNum=30
    lastMinNum=-15
    tradeDF=pd.DataFrame()
    InitialTradeDetail=pd.Series({'Cash':Init['Cash'],
                                     'openInterest':Init['openInterest'],
                                     'margin':0,
                                     'marginCall':0})
    for thisDate in bar(dateIndex[-200:]):
        minIndex=allMinIndex[(allMinIndex-pd.Timedelta('3H')).date ==thisDate][startMinNum:lastMinNum]
        if len(minIndex)==0:
            continue
        thisDayTradeDF=tradeDetail(minIndex,InitialTradeDetail)
        thisDayStartCash = thisDayTradeDF.iloc[0]['Cash']
        for thisMin in minIndex[1:]:
            LastMin = thisDayTradeDF[thisDayTradeDF.index < thisMin].dropna(how='all').index.values[-1]
            thisDayTradeDF['openInterest'][thisMin] = thisDayTradeDF['openInterest'][LastMin]
            thisDayTradeDF['Cash'][thisMin] = thisDayTradeDF['Cash'][LastMin]

            LastMinLong = 1 if thisDayTradeDF['openInterest'][LastMin] > 0 else -1 if thisDayTradeDF['openInterest'][LastMin] < 0 else 0
            if LastMinLong != 0:
                LastMinPnL = thisDayTradeDF['openInterest'][LastMin] * contractMultiplier * (openSeries[thisMin] - openSeries[LastMin])
                thisDayTradeDF['Cash'][thisMin] = thisDayTradeDF['Cash'][thisMin] + LastMinPnL

            Ret2ThisMin=thisDayTradeDF['Cash'][thisMin]/thisDayStartCash-1
            Ret2ThisMax=thisDayTradeDF['Cash'][thisMin]/thisDayTradeDF['Cash'].max()-1
            positionSeries[thisMin:minIndex[-1]]= 0 if Ret2ThisMax<-stopwin or Ret2ThisMin<-stoploss  else positionSeries[thisMin:minIndex[-1]]#or thisMin==minIndex[-1]
            thisOpenInterest = int(thisDayTradeDF['Cash'][thisMin] * positionSeries[thisMin] / openSeries[thisMin] / contractMultiplier)
            isSameSignal=(thisOpenInterest*thisDayTradeDF['openInterest'][LastMin])>0
            if isSameSignal:
                tradeVolume=0
            else:
                thisDayTradeDF['openInterest'][thisMin] = thisOpenInterest
                tradeVolume=abs(thisOpenInterest - thisDayTradeDF['openInterest'][LastMin])
            thisDayTradeDF['Cash'][thisMin] = thisDayTradeDF['Cash'][thisMin] - (Fee + Slippage)*tradeVolume* openSeries[thisMin]*contractMultiplier
            thisDayTradeDF['margin'][thisMin] = abs(thisDayTradeDF['openInterest'][thisMin] * openSeries[thisMin] * contractMultiplier * ExchMarginRatioSeries[thisMin])

        thisDayOpenMarginCall = max(thisDayTradeDF['margin'][thisMin] - thisDayTradeDF['Cash'][thisMin], 0)
        if thisDayOpenMarginCall > 0:
            thisDayTradeDF['marginCall'][thisMin] = thisDayTradeDF['marginCall'][thisMin] + thisDayOpenMarginCall
            thisDayTradeDF['Cash'][thisMin] = thisDayTradeDF['Cash'][thisMin] + thisDayOpenMarginCall
        InitialTradeDetail=thisDayTradeDF.iloc[-1]
        tradeDF=tradeDF.append(thisDayTradeDF)
    return tradeDF

if __name__=='__main__':
    backtest_freq = 'min'
    contractName = 'rb'
    backtestData=getBackTestData(backtest_freq,contractName)
    signals=Dual_Trust(contractName,1,0.5,0.5)
    positionSeries=getPosition(leverage=2,signals=signals)
    Init={'Cash': 1000000, 'openInterest': 0}

    Fee=0.0005
    contractMultiplier=10
    Slippage=0
    stoploss=0.01
    stopwin=0.01
    tradeDetail=CalRealTradingDetail(positionSeries, backtestData,Init,Fee=Fee,contractMultiplier=contractMultiplier,ExchMarginRatioSeries=None, Slippage=Slippage,stoploss=stoploss,stopwin=stopwin)
    tradeDetail.Cash.plot()
    plt.show()